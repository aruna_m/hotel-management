# Business objects are declared as Python classes
from openerp.osv import osv,fields


class hotelmanage(osv.osv):
	_name="hotel.management"
	_columns={
		'name': fields.char('Title', size=64, required=True, translate=True),
		'state': fields.selection([('draft','Draft'),
		('confirmed','Confirmed')],'State'),
		# Description is read-only when not draft!
		'description': fields.text('Description', readonly=True,
		states={'draft': [('readonly', False)]} ),
		# 'active': fields.boolean('Active'),
		'invent_date': fields.date('Invent date'),
		# by convention, many2one fields end with '_id'
		'inventor_id': fields.many2one('res.partner','Inventor'),
		'inventor_country_id': fields.char('country'),
		'date_start':fields.date('date start'),
		'date_stop':fields.datetime('date stop'),
		'breed':fields.char('Breed'),
		'image':fields.binary('image'),

		}
	def generate_quotation_menu(self,cr,uid,ids,context=None):
		vals={}
		# raise osv.except_osv(_('warning'), _(' Your message'))
		obj1=self.pool.get('hotel.management').search(cr,uid,[('inventor_id','in',ids)])
		obj2=self.browse(cr,uid,ids)
		
		vals['name']=obj2.name
		vals['image']=obj2.image
		vals['inventor_id']=obj2.inventor_id.id
		vals['breed']=obj2.breed
		res_id=self.pool.get('room.booking').create(cr,uid,vals,context=None)
		
		return {  
		'name': 'Creating Quotation',
		'view_mode': 'form',
		'view_id': False,
		'view_type': 'form',
		'res_model': 'room.booking',
		'res_id':res_id,
		'type': 'ir.actions.act_window',
		'context': {},
		'target': 'new',
	}
	

hotelmanage()
# adding a comment

