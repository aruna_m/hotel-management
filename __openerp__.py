# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Hotel Management',
    'version': '1.1',
    'category': 'Hotel Management',
    
    'summary': 'Dinner booking, Room booking, staff,crew',
    'description': """


    """,
    'author': 'OpenERP SA',
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['base','sale','hr',],
    'data': [
    'view/dinnerbooking_view.xml',
    'view/roombooking_view.xml',
    # 'view/sequense_view.xml',
    # 'view/custamers_view.xml',
        

        
    ],
  
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
